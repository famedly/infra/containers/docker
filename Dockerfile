FROM golang:1.15-buster
RUN echo "deb http://deb.debian.org/debian buster-backports main" > /etc/apt/sources.list.d/complement.list && apt-get update && apt-get install -y libolm3 libolm-dev/buster-backports
RUN curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh
ADD https://github.com/matrix-org/complement/archive/master.tar.gz .
RUN tar -xzf master.tar.gz && cd complement-master && go mod download 

VOLUME [ "/ca" ]

# Docker in docker setup
RUN mkdir /certs /certs/client && chmod 1777 /certs /certs/client
ENV DOCKER_TLS_CERTDIR=/certs
ENV DOCKER_HOST=tcp://docker:2376
ENV DOCKER_TLS_VERIFY=1
ENV DOCKER_CERT_PATH=/certs/client
